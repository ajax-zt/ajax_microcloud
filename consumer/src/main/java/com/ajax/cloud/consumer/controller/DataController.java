package com.ajax.cloud.consumer.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@RestController
public class DataController {
    @Bean
    @LoadBalanced
    public RestTemplate rest() {
        return new RestTemplate();
    }

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping(path = "/data")
    public String getUser() {
        String dto = restTemplate
                .getForObject("http://client/getData", String.class);
        return dto;
    }
}