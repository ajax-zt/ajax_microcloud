package com.ajax.cloud.client1.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CommonController {

    @RequestMapping(value = "/getData")
    public String getTestData(){
        return "hello springCloud by client1";
    }
}
