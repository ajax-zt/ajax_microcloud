package com.ajax.cloud.client2.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CommonController {

    @RequestMapping(value = "/getData")
    public String getTestData(){
        return "hello springCloud by client2";
    }
}
